<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Respondent extends Model
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $dates = ['dob', 'opt_in_at'];

    public function cluster()
    {
        return $this->belongsTo(Cluster::class);
    }
}
