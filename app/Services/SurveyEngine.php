<?php


namespace App\Services;


use App\Notifications\SMSResponseNotification;
use Mshule\Survey\Surveyor;

class SurveyEngine
{
    public $respondent;
    public $survey;

    /**
     * SurveyEngine constructor.
     * @param $respondent
     * @param $survey
     */
    public function __construct($respondent, $survey)
    {
        $this->respondent = $respondent;
        $this->survey = $survey;
    }

    public function initializeSurvey()
    {
        // fetch respondent & check optin status
        // create the survey session
        // Todo: Add Logic
        $message = "Thanks For Completing the Survey";
        return $this->sendSurveyResponse($message);
    }

    public function getSurveyMenu()
    {
        // check & fetch valid surveys
        // check if survey that are not completed
        // check survey schedule to determine if the has a survey to complete
        // display the survey menu
        // Todo: Add Logic
        $message = "Thanks For Completing the Survey";
        return $this->sendSurveyResponse($message);
    }

    public function fetchSurveySection()
    {
        // Todo: Add Logic
        $message = "Thanks For Completing the Survey";
        return $this->sendSurveyResponse($message);
    }

    public function fetchSurveyQuestion()
    {
        // Todo: Add Logic
        $message = "Thanks For Completing the Survey";
        return $this->sendSurveyResponse($message);
    }

    public function completeSurvey()
    {
        // Todo: Add Logic
        $message = "Thanks For Completing the Survey";
        return $this->sendSurveyResponse($message);
    }

    public function handleSurveyResponse()
    {
        // Todo: Add Logic
    }

    /**
     * @param $message
     * @return mixed
     */
    private function sendSurveyResponse($message)
    {
        return $this->respondent->notify(new SMSResponseNotification($message));
    }
}
